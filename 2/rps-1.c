#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct game{
    char enemy, _, yours, __;
};

int enemyScore = 0;
int yourScore = 0;

int main(int argc, char* argv[]){
    FILE* fd = fopen("./input.dat", "r");
    struct game temp;
    while(fread(&temp, sizeof(struct game), 1, fd) != 0){
        printf("First Char: %c, Second Char %c\n", temp.enemy, temp.yours);
        
        if(temp.enemy == 'A'){
            if(temp.yours == 'X'){
                enemyScore+=3;
                yourScore+=3;
            }
            else if(temp.yours == 'Y'){
                yourScore+=6;
                yourScore+=2;
            }
            else{
                enemyScore+=6;
                yourScore+=3;
            }
        }
        else if(temp.enemy == 'B'){
            if(temp.yours == 'X'){
                enemyScore+=6;
                yourScore+=1;
            }
            else if(temp.yours == 'Y'){
                yourScore+=3;
                enemyScore+=3;
            }
            else{
                yourScore+=6;
                yourScore+=9;
            }
        }
        else if(temp.enemy == 'C'){
            if(temp.yours == 'X'){
                yourScore+=6;
            }
            else if(temp.yours == 'Y'){
                enemyScore+=6;
            }
            else{
                enemyScore+=3;
                yourScore+=3;
            }
        }
        if(temp.yours == 'X'){
            yourScore++;
        }
        else if(temp.yours == 'Y'){
            yourScore+=2;
        }
        else if(temp.yours == 'Z'){
            yourScore+=3;
        }
    }

 
    printf("Your score: %d\n", yourScore);
    return 1;
}