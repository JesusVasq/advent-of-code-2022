import java.util.*;
import java.io.*;

public class Dirs2{

    public static class aFile{
        public String fileDir;
        public boolean isDir;
        public int size;
        public aFile parent;
        public ArrayList<aFile> files = new ArrayList<aFile>();
        public aFile(String fileDir, boolean isDir, int size, aFile parent){
            this.fileDir = fileDir;
            this.isDir = isDir;
            this.size = size;
            this.parent = parent;
        }

        public void addFile(aFile temp){
            addSize(temp.size);
            this.files.add(temp);
        }

        public void addSize(int fileSize){
            this.size += fileSize;
            if(this.parent != null){
                this.parent.addSize(fileSize);
            }
        }
    }

    public static final int total = 70000000;
    public static final int needed = 30000000;
    public static int used = 0;
    public static Scanner myScanner;
    public static aFile root = new aFile("/", true, 0, null);
    public static aFile cwd = null;

    public static void main(String[] args) {
        try {
            myScanner = new Scanner(new File("input.dat"));    
        } catch (FileNotFoundException e) {
            System.err.print("Could not find file.");
            System.exit(0);
        }

        while(myScanner.hasNextLine()){
            String nextLine = myScanner.nextLine();
            // issuing command
            if(nextLine.charAt(0) == '$'){
                if(nextLine.substring(2,4).equals("cd")){
                    // System.out.println("WE GOT CD");
                    String nextDirectory = nextLine.substring(5,nextLine.length());
                    if(nextDirectory.equals("/")){
                        cwd = root;
                    }
                    else if(nextDirectory.equals("..")){
                        cwd = cwd.parent;
                    }
                    else{
                        for(aFile temp: cwd.files){
                            if(temp.fileDir.equals(nextDirectory)){
                                cwd = temp;
                            }
                        }
                    }
                }
                else if(nextLine.substring(2,4).equals("ls")){
                    // DO NOTHING
                }
            }
            else{
                String[] splitStr = nextLine.split(" ");
                if(splitStr[0].equals("dir")){
                    aFile newDir = new aFile(splitStr[1], true, 0, cwd);
                    cwd.addFile(newDir);
                }
                else {
                    aFile newFile = new aFile(splitStr[1], false, Integer.parseInt(splitStr[0]), cwd);
                    cwd.addFile(newFile);
                }
            }
        }
        cwd = root;
        partTwo();
        
    }
    public static void partTwo(){
        ArrayList<Integer> dirs = new ArrayList<Integer>();
        used = root.size;
        int freeSpace = total - used;
        int neededSpace = needed - freeSpace;
        System.out.println("Total Root: " + root.size);
        System.out.println("Free: " + freeSpace);
        System.out.println("Needed: " + neededSpace);
        findDirs(neededSpace, dirs, cwd);
        Collections.sort(dirs);
        for(int i = 0; i < dirs.size(); i ++){
            System.out.println(i + ": " + dirs.get(i));
        }
        System.out.println("Smallest folder for deletion: " + dirs.get(0));
    }

    public static void findDirs(int needed, ArrayList<Integer> dirs, aFile mFile){
        if(mFile.isDir){
            if(mFile.size >= needed ){
                dirs.add(mFile.size);
            }
            for(aFile temp: mFile.files){
                findDirs(needed, dirs, temp);
            }
        }
    }
}

