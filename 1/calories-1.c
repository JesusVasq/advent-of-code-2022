
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]){
    FILE* fp = fopen("./input.dat", "r");
    char buffer[128];
    int max = 0;
    int currentSum = 0;
   
    while(fgets(buffer, 128, fp)){
        if(!strcmp(buffer,"\n")){
            if(currentSum > max)
                max = currentSum;
            currentSum = 0;
        }
        currentSum+=atoi(buffer);
    }
    if(currentSum > max)
        max = currentSum;
    printf("Max: %d\n", max);
    return 1;
}