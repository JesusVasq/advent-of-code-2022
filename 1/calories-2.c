
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int cmpfunc (const void * a, const void * b) {
   return ( *(int*)b - *(int*)a );
}

int main(int argc, char *argv[]){
    FILE* fp = fopen("./input.dat", "r");
    char buffer[128];
    int currentSum = 0;
    int size = 0;
    int* numsArray = (int *) malloc(sizeof(int));
    while(fgets(buffer, 128, fp)){
        if(!strcmp(buffer,"\n")){
            size++;
            // really inefficient mallocing after every new sum but idc
            numsArray = (int *) realloc(numsArray, sizeof(int) * size);
            numsArray[size-1] = currentSum;
            currentSum = 0;
        }
        currentSum+=atoi(buffer);
    }

    qsort(numsArray,size, sizeof(int), cmpfunc);
    int topThree = numsArray[0] + numsArray[1] + numsArray[2];
    printf("Top Three sum: %d", topThree);
    return 1;
}